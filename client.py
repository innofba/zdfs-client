#!/usr/bin/env python

import json
import sys

import requests


def list_files(dir):
    r = requests.get(config["name_node_addr"] + "/ls", params={"path": dir})

    if r.status_code != 200:
        raise Exception(r.json()["message"])

    files = r.json()
    for file in files:
        yield file["path"], file["type"], file["content"]


def remove_file(path):
    r = requests.get(config["name_node_addr"] + "/rm", params={"path": path})

    if r.status_code != 200:
        raise Exception(r.json()["message"])


def remove_dir(path):
    for fullpath, type, content in list_files(path):
        if type == 'zdfs/directory':
            remove_dir(fullpath)
        elif type == 'zdfs/file':
            remove_file(fullpath)
            print("rm", fullpath)
    remove_file(path)


# ● Initialize the client storage, should remove any existing file in the dfs root dir and return available size.
def init(args: list):
    try:
        r = requests.get(config["name_node_addr"] + "/init")

        if r.status_code != 200:
            raise Exception(r.json()["message"])

        print("ZDFS initialised\nNodes:")

        df([])

    except Exception as e:
        print("ERROR:", e)


# ● File read. Should allow to read any file from DFS (download a file from the DFS to the Client side).
def get(args: list):
    node_path, local_path = args[0], args[1]
    # Redirects are allowed by default
    try:
        r = requests.get(config["name_node_addr"] + "/get", params={"path": node_path})

        if r.status_code != 200:
            raise Exception(r.json()["message"])

        with open(local_path, 'wb') as fd:
            fd.write(r.content)
            fd.flush()

        print("OK", local_path)
    except Exception as e:
        print("ERROR:", e)


# ● File write. Should allow to put any file to DFS (upload a file from the Client side to the DFS)
def post(args: list):
    local_path, node_path = args[0], args[1]
    with open(local_path, 'rb') as fd:
        try:
            r = requests.post(url=config["name_node_addr"] + "/upload", data={"path": node_path}, files={"file": fd})

            if r.status_code != 200:
                raise Exception(r.json()["message"])

            print("OK")
        except Exception as e:
            print("ERROR:", e)


# ● File/dir delete. Should allow to delete any file/dir from DFS
def rm(args: list):
    node_path = args[0]
    try:
        remove_file(node_path)
        print("OK")
    except Exception as e:
        if str(e) == "Directory is not empty":
            print("Directory is not empty. Do you want to recursively delete it?")
            if input("'y' for yes, any other for no > ").lower().strip() == 'y':
                remove_dir(node_path)
            return
        print("ERROR:", e)


# ● File copy. Should allow to create a copy of file.
def cp(args: list):
    old_node_path, new_node_path = args[0], args[1]
    try:
        r = requests.get(config["name_node_addr"] + "/cp", params={"from": old_node_path,
                                                                   "to": new_node_path})

        if r.status_code != 200:
            raise Exception(r.json()["message"])

        print("OK")
    except Exception as e:
        print("ERROR:", e)


# ● File move. Should allow to move a file to the specified path.
def mv(args: list):
    old_node_path, new_node_path = args[0], args[1]
    try:
        r = requests.get(config["name_node_addr"] + "/mv", params={"from": old_node_path,
                                                                   "to": new_node_path})

        if r.status_code != 200:
            raise Exception(r.json()["message"])

        print("OK")
    except Exception as e:
        print("ERROR:", e)


# ● Read directory. Should return list of files, which are stored in the directory.
def ls(args: list):
    folder_path = args[0]
    try:
        for fullpath, type, content in list_files(folder_path):
            if type == 'zdfs/file':
                filehash, size, nodes = content
                print(fullpath + "\t" + size + "b\t" + nodes)
            else:
                print(fullpath)
    except Exception as e:
        print("ERROR:", e)


# ● Make directory. Should allow to create a new directory.
def mkdir(args: list):
    folder_path = args[0]
    try:
        r = requests.get(config["name_node_addr"] + "/mkdir", params={"path": folder_path,
                                                                      "mkdir": True})

        if r.status_code != 200:
            raise Exception(r.json()["message"])

        print("OK")
    except Exception as e:
        print("ERROR:", e)


def df(args: list):
    try:
        r = requests.get(config["name_node_addr"] + "/availableSpace")

        if r.status_code != 200:
            raise Exception(r.json()["message"])

        total_space = 0

        for node in r.json():
            total_space += node['third']
            print(node['first'] + "\t" + str(node['third']) + "b\t" + node['second'])
        print("Total free disk space:", total_space, "bytes")

    except Exception as e:
        print("ERROR:", e)


def dzfs_commands_help():
    print("Available commands:")
    print("\tget remote_path local_path")
    print("\tpost local_path remote_path")
    print("\trm path")
    print("\tmv old_path new_path")
    print("\tls path")
    print("\tmkdir new_dir_path")
    print("\tdf")
    print("\texit")


def save_config(data: dict):
    with open('config.json', 'w') as fd:
        json.dump(data, fd)


def execute(cmd):
    if cmd[0] == 'help':
        dzfs_commands_help()
    elif cmd[0] not in valid_commands.keys() or len(cmd) < 1:
        print("Command not found")
    elif len(cmd) - 1 != valid_commands.get(cmd[0])[1]:
        print("Invalid number of arguments")
    else:
        valid_commands.get(cmd[0])[0].__call__(cmd[1:])


if __name__ == '__main__':
    # Declare config with defaults
    config = {"name_node_addr": 'http://127.0.0.1:8080'}

    # Read cmd-line args
    if len(sys.argv) >= 3:
        config["name_node_addr"] = "http://" + str(sys.argv[1]) + ':' + str(sys.argv[2])
        save_config(config)
    else:
        print("zdfs host port [cmd]")
        sys.exit(0)

    # What to call, how many args does it need
    valid_commands = {
        'init': [init, 0],
        'get': [get, 2],
        'post': [post, 2],
        'rm': [rm, 1],
        'mv': [mv, 2],
        'cp': [cp, 2],
        'ls': [ls, 1],
        'df': [df, 0],
        'mkdir': [mkdir, 1]
    }

    if len(sys.argv) > 3:
        execute(sys.argv[3:len(sys.argv)])
    else:
        # Start.
        print("Welcome to ZDFS.")
        dzfs_commands_help()
        while 1:
            print("> ", end="")
            cmd = str(input()).lower().split(" ")
            if cmd[0] == 'exit':
                break
            execute(cmd)
        print("Goodbye!")
